# README #

### What is this repository for? ###
This repository is a study example of TDFT elastic wave-modeling for seismic applications.

* 2D Elastic modeling using finite-difference and the staggered grid method described by Virieux 1986.
* Implements CPML described by Dimitri Komatitsch and Roland Martin, An unsplit convolutional Perfectly Matched Layer improved at grazing incidence for the seismic wave equation, Geophysics, vol. 72(5), p SM155-SM167, doi: 10.1190/1.2757586 (2007)
* Object oriented implementation

### How do I get set up? ###

* Matlab search path to span folder and subfolders
* Run the FD_Elastic_2D.m file in the examples folder

![alt text](examples/FD_marmousi_elastic2D.png)
