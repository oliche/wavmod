function x = flatten(x,ndim)
% x = flatten(x)
% inline command for x = x(:);

x = x(:);
    