classdef transducer < handle
    %TRANSDUCER interface for source and receiver objects in wavefield
    % MIT License
    % Copyright (c) 2017 Olivier Winter
    % https://en.wikipedia.org/wiki/MIT_License
    %modeling tools
    
    properties (Abstract)
        X
        Y
        Z
        IndX
        IndY
        IndZ
    end
    
    methods
        function ComputeIndices(self, EM)
            % self.ComputeIndices(EM)
            % returns indices corresponding to X,Y,Z in the model EM provided
            self.IndX = min(max(1, round(self.X./ EM.dx)), EM.nx); 
            self.IndY = min(max(1, round(self.Y./ EM.dy)), EM.ny); 
            self.IndZ = min(max(1, round(self.Z./ EM.dz)), EM.nz); 
        end
    end
    
end

