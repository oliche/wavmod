classdef acoustic_isotropic < wavmod.models.interface
    % EM = wavmod.models.elastic_isotropic(mesh_siz, Rho, 'Vp',Vp); % Seismic Velocities
    % MIT License
    % Copyright (c) 2016 Olivier Winter
    % https://en.wikipedia.org/wiki/MIT_License
    properties (Constant)
        type ='acoustic'
        trope = 'isotropic'
        components = {'Rho','Vp'} % active components
    end
    
    properties
        Rho   % density
        dx    % meshsize in meters
        dy    % meshsize in meters
        dz    % meshsize in meters
        nx
        ny
        nz
        Vp % P-wave velocity
        ndim
        vmin % minimum velocity in the model
    end
    
    methods
        %% Constructor
        function obj = acoustic_isotropic(mesh_siz , Rho,Vp)
            % EM = wavmod.models.acoustic_isotropic(mesh_siz, Rho,Vp);
            obj.Rho = Rho; clear('Rho');
            obj.Vp = Vp; clear('Vp');
            % Prepare meshsizes and dims
            siz = size(obj.Rho);
            assert(length(siz) ==2  | length(siz) ==3) % at least 2D
            assert(all(siz > 1)) % no dummy dimension
            obj.ndim = length(siz);
            [obj.dz, obj.dy, obj.dx ] = deal(mesh_siz);
            obj.nz = siz(1);
            obj.ny = siz(2);
            obj.nx = siz(end);
            if obj.ndim ==2, [obj.dy, obj.ny] = deal(1); end
            obj.vmin = min(min(flatten(nonzeros(obj.Vp))) , min(flatten(nonzeros(obj.Vp))));
        end
        %% Superclass methods        
        function ShowModel(obj,field)
            if nargin ==1, field =''; end
            ShowModel@wavmod.models.interface(obj,field);
        end
        function x = xscale(obj), x = xscale@wavmod.models.interface(obj); end
        function y = yscale(obj), y = yscale@wavmod.models.interface(obj); end
        function z = zscale(obj), z = zscale@wavmod.models.interface(obj); end
        function addWhiteNoiseShallow(self,zmax, percent)
            addWhiteNoiseShallow@wavmod.models.interface(self,zmax, percent);
        end
        function resample_interp(self,dzxy)
            resample_interp@wavmod.models.interface(self,dzxy);
        end
        function modfun(self,fcn)
            modfun@wavmod.models.interface(self,fcn);
        end
        %% Superclass overloads
        function Si = Si(self, order)
            if nargin <=1 , order=5; end
            Si = wavmod.pde2.stableSampleInterval(order, min([self.dx self.dz]), max(self.Vp(:)), self.ndim);
        end
    end
end