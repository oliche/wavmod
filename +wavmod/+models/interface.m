classdef interface < handle
    % wavmod.models.interface
    % MIT License
    % Copyright (c) 2016 Olivier Winter
    % https://en.wikipedia.org/wiki/MIT_License
    
    properties (Abstract= true , Constant=true)
        type  % elastic | acoustic | visco-elastic
        trope % isotropic | anisotropic
        components % fields of the model (2 for acoustic: {Rho,Vp}, 3 for elastic, etc..)
    end
    
    properties (Abstract)
        Rho   % density
        dx    % meshsize in meters
        dy    % meshsize in meters
        dz    % meshsize in meters
        nx   
        ny
        nz
        ndim
    end

    methods
        function ShowModel(self,field)
            % self.ShowModel('Rho')
            % self.ShowModel({'Vp', 'Vs'})
            % If no input field, display all attributes of model
            if isempty(field)
                field = unique([self.components(:) ; {'Vp' ; 'Vs'}]);
            end
            % If single input char, convert to cell so the loop works
            if ~iscell(field), field = {field}; end
            siz = [self.nz self.nx self.ny]; siz(siz==1)=[];
            xlab = self.xscale.*self.dx;
            zlab = self.zscale.*self.dz;
            ylab = self.yscale.*self.dy;
            % loop over the fields
            for m = 1 : length(field)
                if ~( isprop(self, field{m}) || ismethod(self,field{m})), continue, end
                sf = size(self.(field{m}));
                if length(sf)~=length(siz) || ~all(sf==siz), continue, end
                switch self.ndim
                    case 2, disp2d
                    case 3, disp3d
                end
            end
            % nested function display 3d
            function disp3d
                yslice = round((self.ny/2));
                xslice = round((self.nx/2));
                zslice = round((self.nz/2));
                IM = self.(field{m});
                figure,
                imagesc( xlab,ylab, squeeze( IM(zslice,:,:)) ,'Parent',subplot(2,2,3))
                axis('equal'), xlabel('x'), ylabel('y'), title([field{m} '@ z = ' num2str(zslice*self.dz)])
                imagesc( xlab,zlab, squeeze( IM(:,xslice,:)) ,'Parent',subplot(2,2,2))
                axis('equal'), xlabel('y'), ylabel('z'), title([field{m} '@ x = ' num2str(xslice*self.dx)])
                imagesc( xlab,zlab, squeeze( IM(:,:,yslice)) ,'Parent',subplot(2,2,1))
                axis('equal'), xlabel('x'), ylabel('z'), title([field{m} '@ y = ' num2str(yslice*self.dy)])
            end
            % nested function display 2d
            function disp2d
                figure, imagesc(xlab,zlab, reshape( self.(field{m}) , [self.nz self.nx.*self.ny]));
                title(field{m}), colorbar
                set(gca,'xaxisLocation','top')
            end
        end
        
        function x = xscale(self), x = ([1:self.nx]-1).*self.dx; end
        function y = yscale(self), y = ([1:self.ny]-1).*self.dy; end
        function z = zscale(self), z = ([1:self.nz]-1).*self.dz; end
        
        function Si = Si(self)
            % Recommended Si for stability (Courant-Friedrichs-Lewy, 1928)
            Si = min([self.dx,self.dz])./sqrt(2)/ max(flatten(self.Vp)) *0.8;
        end
        
        function modfun(self, fcn)
            % self.modfun(fcn)
            % applies the function fcn to all the components of the model,
            % makes sure the output dimensions are updated in the model instance
            for ff = self.components(:)'
                self.(ff{1}) = fcn(self.(ff{1})) ;
            end
            [self.nz, self.nx, self.ny]= size(self.(ff{1}));
        end
            
        function addWhiteNoiseShallow(self,zmax, percent)
            % self.addWhiteNoiseShallow(zmax, percent)
            % Adds a random perturbation in the model from surface to a maximum depth of zmax to create backscatter
            perturb = @(m,p) m+ rand(size(m))*mean(m(:))*p/100 + mean(m(:));
            izmax = min(self.nz,round(zmax/self.dz));
            for ff = self.components(:)'
                self.(ff{1})(1:izmax,:) = perturb(self.(ff{1})(1:izmax,:), percent);
            end
        end
        
        
        function resample_interp(self,dzxy)
            % self.resample([dz,dx,dy])
            % self.resample([2.5,2.5]) % resamples the model to 2.5 meters in depth and x
            dzxy_orig = [self.dz,self.dx,self.dy];
            % for each component of the model
            for ff = self.components(:)'
                % loop over each dimension to be resampled
                for dim = 1:length(dzxy)
                    n = size(self.(ff{1}),dim);
                    fac = dzxy./dzxy_orig(dim);
                    if fac==1 | n==1, continue, end
                    permutation  = [dim setxor([1:3], dim)];
                    self.(ff{1}) = ipermute(interp1([1:n]',  permute( self.(ff{1}),permutation) ,[1:fac:n]'),permutation);
                end
            end
            % Update the properties of the model
            [self.nz, self.nx, self.ny]= size(self.(ff{1}));
            try, self.dz = dzxy(1); self.dx = dzxy(2); self.dz = dzxy(3); end
        end
        
    end
    
    methods (Static)
        function [L,Mu] = VpVs_2_LambdaMu(Rho,Vp,Vs)
           Mu = Rho .* (Vs.^2) ;
           L  = Rho .* (Vp.^2 - 2.*Vs.^2);
        end
        
        function [Vp,Vs] = LambdaMu_2_VpVs(Rho,L,Mu)
        Vp = sqrt( (L+2.*Mu)./Rho);
        Vs = sqrt(Mu./Rho);
        end
    end
    
end