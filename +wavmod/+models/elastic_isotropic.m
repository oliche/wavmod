classdef elastic_isotropic < wavmod.models.interface
    % EM = wavmod.models.elastic_isotropic(mesh_siz, Rho, 'L',L,'Mu',Mu); % Lam� parameters
    % EM = wavmod.models.elastic_isotropic(mesh_siz, Rho, 'Vp',Vp,'Vs',Vs); % Seismic Velocities
    % MIT License
    % Copyright (c) 2016 Olivier Winter
    % https://en.wikipedia.org/wiki/MIT_License
    properties (Constant)
        type ='elastic'
        trope = 'isotropic'
        components = {'Rho','Mu','L'} % active components
    end
    
    properties
        % Superclass
        Rho   % density
        dx    % meshsize in meters
        dy    % meshsize in meters
        dz    % meshsize in meters
        nx
        ny
        nz
        % Proper
        L % Lambda from Lam� Parameters
        Mu % Mu from Lam� Parameters
        ndim
        vmin % minimum velocity in the model
    end
    
    methods
        %% Constructor
        function obj = elastic_isotropic(mesh_siz , Rho,varargin)
            % EM = wavmod.models.elastic_isotropic(mesh_siz, Rho, 'L',L,'Mu',Mu); % Lam� parameters
            % EM = wavmod.models.elastic_isotropic(mesh_siz, Rho, 'Vp',Vp,'Vs',Vs); % Seismic Velocities
            p=inputParser ;
            p.addParamValue('L',[]) ;
            p.addParamValue('Mu',[]) ;
            p.addParamValue('Vp',[]) ;
            p.addParamValue('Vs',[]) ;
            p.parse(varargin{:}) ; field = fieldnames(p.Results) ;
            for r = 1 : length(field), eval([field{r} '= p.Results.(field{r}) ; ']) ; end
            % If Vp/Vs provided, convert to Lambda/Mu
            if isempty(Mu) & isempty(L)
                [obj.L,obj.Mu] = wavmod.models.interface.VpVs_2_LambdaMu(Rho,Vp,Vs);
                clear('Vp','Vs')
            else
                obj.L = L; clear('L');
                obj.Mu = Mu; clear('Mu');
            end
            clear('Vp','Vs')
            obj.Rho = Rho; clear('Rho');
            % Once we have Lambda /Mu, prepare meshsizes and dims
            siz = size(obj.Mu);
            assert(length(siz) ==2  | length(siz) ==3) % at least 2D
            assert(all(siz > 1)) % no dummy dimension
            obj.ndim = length(siz);
            [obj.dz, obj.dy, obj.dx ] = deal(mesh_siz);
            obj.nz = siz(1);
            obj.ny = siz(2);
            obj.nx = siz(end);
            if obj.ndim ==2, [obj.dy, obj.ny] = deal(1); end
            obj.vmin = min([flatten(nonzeros(obj.Vs)) ; flatten(nonzeros(obj.Vp))]);
        end
        %% Superclass methods        
        function ShowModel(obj,field)
            if nargin ==1, field =''; end
            ShowModel@wavmod.models.interface(obj,field);
        end
        function x = xscale(obj), x = xscale@wavmod.models.interface(obj); end
        function y = yscale(obj), y = yscale@wavmod.models.interface(obj); end
        function z = zscale(obj), z = zscale@wavmod.models.interface(obj); end
        function addWhiteNoiseShallow(self,zmax, percent)
            addWhiteNoiseShallow@wavmod.models.interface(self,zmax, percent);
        end
        function Si = Si(self)
            Si = Si@wavmod.models.interface(self);
        end
        function resample_interp(self,dzxy)
            resample_interp@wavmod.models.interface(self,dzxy);
        end
        function modfun(self,fcn)
            modfun@wavmod.models.interface(self,fcn);
        end
        %% Class methods
        function Vp = Vp(self)
            % Compute the Vp volume on the fly
            Vp = sqrt( (self.L+2.*self.Mu)./self.Rho);
        end
        function Vs = Vs(self)
            % Compute the Vs volume on the fly
            Vs = sqrt(self.Mu./self.Rho);
        end
    end
end