function [OP,c0,c] = derivativeOperator(N,dim)
% MIT License
% Copyright (c) 2016 Olivier Winter
% https://en.wikipedia.org/wiki/MIT_License
%   [OP,c0,c] = wavmod.pde2.derivativeOperator(N,dim)
%   2D and 3D operators for second derivatives of spatial order 2N 
%   OP : derivative
%   c0 : central Cn coefficient
%   c  : remaining Cn coefficients
%   See Liu and Sen 2009 "Advanced finite-difference methods for seismic modelling", formula (21)
%   Note that formula (21) is correct for cn but incorrect for c0. 
%   The correct c0 formula is given later in A6.
%   cf. also https://en.wikipedia.org/wiki/Finite_difference_coefficient
%

%% Compute coefficients
c=zeros(N,1);
for n=1:N
    prod=1;
    for m=1:n-1
        prod=prod*abs(m^2/(m^2-n^2));
    end
    for m=n+1:N
        prod=prod*abs(m^2/(m^2-n^2));
    end
    c(n)=((-1)^(n+1)/n^2)*prod;
end
c0=-2*sum(c); % Liu's paper does not have the correct sign in (21)

%% Compute Convolution Operator
if nargin ==1, dim = 1; end
kernel = [flipud(c(:)) ; c0(:) ;  c(:)];
OP = kernel;
% higher dimension for acoustic isotropic case where all spatial
% derivatives fit in a single N-Dims operator
if dim >=2
    OP = cat(2,zeros(N*2+1,N) , OP, zeros(N*2+1,N));
    OP(N+1,:) = OP(N+1,:) + permute( kernel, [2 1]);
end
if dim >=3
    OP = cat(3,zeros(N*2+1,N*2+1,N) , OP, zeros(N*2+1,N*2+1,N));
    OP(N+1,N+1,:) = OP(N+1,N+1,:)  + permute( kernel, [3 2 1]);
end

