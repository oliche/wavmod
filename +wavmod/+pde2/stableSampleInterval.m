function [ Si ] = stableSampleInterval( N, dx, vmax, dim)
% MIT License
% Copyright (c) 2016 Olivier Winter
% https://en.wikipedia.org/wiki/MIT_License
% wavmod.pde2.stableSampleInterval(order, dx, vmax, dim)
% Calculates the stability limit of the arbitrary order EFDM
% Based on the paper: "Advanced finite difference 
% methods for seismic modeling" Liu and Sen 2009 formula (16).
%
% This summation is also the base for computing 2D and 3D EFDM stability
% see Liu and Sen "A new time space domain high-order finite-difference
% method for the AWE" formula 60 and 61.

[~,~,c] = wavmod.pde2.derivativeOperator(N,dim);
 
n=floor((N+1)/2);
stability = 1./sqrt(sum(c(2.*[1:n]-1)));
Si = 0.9 * dx.*stability ./ (sqrt(dim).*vmax);

