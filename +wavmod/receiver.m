classdef receiver < wavmod.transducer
    %wavmod.receiver
    % MIT License
    % Copyright (c) 2017 Olivier Winter
    % https://en.wikipedia.org/wiki/MIT_License   
    properties
        X
        Y
        Z
        IndX
        IndY
        IndZ
    end
    
    methods
        %% Constructor
        function self = receiver(varargin)
            % RCV = wavmod.receiver('X',X,'Y',Y)
            p=inputParser;
            p.addParamValue('X',    self.X , @isnumeric);
            p.addParamValue('Y',    self.Y , @isnumeric);
            p.addParamValue('Z',    self.Z , @isnumeric);
            p.parse(varargin{:});
            for ff=fields(p.Results)', eval(['self.' ff{1} '=p.Results.' ff{1} ';' ]); end
        end
       %% SuperClass Methods
       function ComputeIndices(self,EM)
            ComputeIndices@wavmod.transducer(self,EM);
       end
    end
    
end

