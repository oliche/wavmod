classdef elastic_isotropic_2d < wavmod.propagators.interface
    % wavmod.propagator.elastic_isotropic
    % MIT License
    % Copyright (c) 2016 Olivier Winter
    % https://en.wikipedia.org/wiki/MIT_License
    % A finite-difference elastic propagator based on the work of Virieux (1986)
    properties (Constant)
        type = 'elastic'    % elastic | acoustic | visco-elastic
        trope= 'isotropic'  % isotropic | anisotropic
        ndims = 2
    end
    
    properties
        RCV             % structure of receivers to sample the wavefield
        SRC             % structure describing source parameters
        EM              % object of type wavmod.models
        OP              % Derivative operator
        Si = NaN        % sample interval secs for simulation
        Si_out = 0.002  % sample interval secs for output
        Rl     = 4      % record length
        Nech
        Nech_out
        % ----------------------------------------
        npml= 15   % size of the perfectly matched layer
        Display = 0 % rate of display
        Verbose = 200 % rate of terminal update
    end
    
    methods
        function self = elastic_isotropic_2d(varargin)
            % elastic_isotropic_2d()
            %% input arguments
            p=inputParser;
            p.addParamValue('EM',  [] , @(x) isa(x, 'wavmod.models.elastic_isotropic'));
            p.addParamValue('RCV', self.RCV);
            p.addParamValue('SRC', self.SRC, @(x) isa(x, 'wavmod.source'));
            p.addParamValue('OP', self.OP, @isnumeric);
            p.addParamValue('Si',    self.Si, @isnumeric);
            p.addParamValue('Si_out',    self.Si_out , @isnumeric);
            p.addParamValue('Rl',    self.Rl , @isnumeric);
            p.addParamValue('npml',  self.npml, @isnumeric);
            p.addParamValue('Display',  self.Display , @isnumeric);
            p.addParamValue('Verbose',  self.Verbose, @isnumeric);
            p.parse(varargin{:});
            for ff=fields(p.Results)', eval(['self.' ff{1} '=p.Results.' ff{1} ';' ]); end
             % Grab a default model if necessary
            if isempty(self.EM), EM =  wavmod_tests.models_library('Marmousi2'); end
            % Once we have a model, init
            self.init();
            % Derivative Operator: first order only for first derivative
            self.OP = struct('dxf',[-1 1]./self.EM.dx,...
                             'dzf', [ -1 ;1]./self.EM.dz, ...
                             'dxb',[0 -1 1]./self.EM.dx,...
                             'dzb', [0; -1 ; 1]./self.EM.dz);
        end        
        function init(self)
            % Invoke the superclass init
            init@wavmod.propagators.interface(self);
        end
        function [Zrec, Xrec, Exx] = run(self)
        % [Zrec, Xrec, Exx] = run(self)
        % 
            %% For a lighter code below, short the self
            SRC = self.SRC;
            RCV = self.RCV;
            OP = self.OP;
            EM = self.EM;
            npml = self.npml;
            Si = self.Si;
            Nech = self.Nech;
            Ntr = length(RCV.X);
            %% Prepare
            PMLX = wavmod.cpml(EM, self, 2);
            PMLZ = wavmod.cpml(EM, self, 1);
            [Sxx, Szz, Sxz, Vx, Vz, XDX, ZDZ, XDZ, ZDX] = deal(EM.Mu.*0);
            [Zrec, Xrec, Exx] = deal(zeros(Nech,Ntr,'single'));
            %% Loop over time
            for m = 1:(Nech)
                %%% Apply Source term
                if m <= length(SRC.WavZ)
                    assert( all( [length(SRC.Z), length(SRC.X)]<=1)) % TODO : non spatially discrete sources
                    Vx(SRC.IndZ,SRC.IndX)= Vx(SRC.IndZ,SRC.IndX) + SRC.WavX(m) .* SRC.AmpX; 
                    Vz(SRC.IndZ,SRC.IndX)= Vz(SRC.IndZ,SRC.IndX) + SRC.WavZ(m) .* SRC.AmpZ; 
                end
                
                %%% Compute stresses
                % Compute spatial derivatives of velocities
                XDX = convn(Vx,OP.dxf,'same');  % d�riv�e spatiale de la vitesse x selon x NO SHIFT (forward)
                ZDZ = convn(Vz,OP.dzb,'same');  % d�riv�e spatiale de la vitesse z selon z SHIFT    (backward)
                XDZ = convn(Vx,OP.dzf,'same');  % d�riv�e spatiale de la vitesse x selon z NO SHIFT (forward)
                ZDX = convn(Vz,OP.dxb,'same');  % d�riv�e spatiale de la vitesse z selon x SHIFT    (backward)
                
                PMLX.Vx =  PMLX.Vx .* PMLX.B + PMLX.A.*XDX(PMLX.ind) ; % PML memory term % Stagger ax,bx X Forward
                PMLZ.Vz =  PMLZ.Vz .* PMLZ.B + PMLZ.A.*ZDZ(PMLZ.ind) ; % PML memory term % No Stagger
                PMLX.Vz =  PMLX.Vz .* PMLX.B + PMLX.A.*ZDX(PMLX.ind) ; % PML memory term % No Stagger
                PMLZ.Vx =  PMLZ.Vx .* PMLZ.B + PMLZ.A.*XDZ(PMLZ.ind) ; % PML memory term % Stagger az, bz Z Forward
                
                XDX(PMLX.ind) = XDX(PMLX.ind) ./ PMLX.K + PMLX.Vx ;  % Application of PML % Stagger Kx, X Forward
                ZDZ(PMLZ.ind) = ZDZ(PMLZ.ind) ./ PMLZ.K + PMLZ.Vz ;  % Application of PML % No Stagger
                ZDX(PMLX.ind) = ZDX(PMLX.ind) ./ PMLX.K + PMLX.Vz ;  % Application of PML % No Stagger
                XDZ(PMLZ.ind) = XDZ(PMLZ.ind) ./ PMLZ.K + PMLZ.Vx ;  % Application of PML % Stagger Kz, Z forward
                
                Sxx = Sxx + ((EM.L + 2.*EM.Mu) .* XDX + EM.L.*ZDZ ) .* Si ;  % Update stresses from derivative of velocities % Stagger L,Mu Forward X
                Szz = Szz + ((EM.L + 2.*EM.Mu) .* ZDZ + EM.L.*XDX ) .* Si ;  % Update stresses from derivative of velocities % Stagger L,Mu Forward X
                Sxz = Sxz +  (EM.Mu       ) .*(XDZ + ZDX)     .* Si;   % Update stresses from derivative of velocities % Stagger Mu   Forward Y
                
                %%% COmpute velocities : Vx
                XDX = convn(Sxx,OP.dxb,'same'); % Compute spatial derivatives of stresses SHIFT (backward)
                XDZ = convn(Sxz,OP.dzb,'same'); % Compute spatial derivatives of stresses SHIFT (backward)
                
                PMLX.Sii =  PMLX.Sii .* PMLX.B + PMLX.A.*XDX(PMLX.ind) ; % PML memory term
                PMLZ.Sij =  PMLZ.Sij .* PMLZ.B + PMLZ.A.*XDZ(PMLZ.ind) ; % PML memory term
                XDX(PMLX.ind) = XDX(PMLX.ind) ./ PMLX.K + PMLX.Sii; % Application of PML
                XDZ(PMLZ.ind) = XDZ(PMLZ.ind) ./ PMLZ.K + PMLZ.Sij; % Application of PML
                
                Vx = Vx + (XDX + XDZ) .* Si ./ EM.Rho;   % Update Velocities from derivative of Stresses, No Stagger Rho
                
                %%% COmpute velocities : Vz
                ZDZ = convn(Szz,OP.dzf,'same'); % Compute spatial derivatives of stresses NO SHIFT (forward)
                ZDX = convn(Sxz,OP.dxf,'same'); % Compute spatial derivatives of stresses NO SHIFT (forward)
                
                PMLX.Sij =  PMLX.Sij .* PMLX.B + PMLX.A.*ZDX(PMLX.ind) ; % PML memory term
                PMLZ.Sii =  PMLZ.Sii .* PMLZ.B + PMLZ.A.*ZDZ(PMLZ.ind) ; % PML memory term
                ZDX(PMLX.ind) = ZDX(PMLX.ind) ./ PMLX.K + PMLX.Sij; % Application of PML
                ZDZ(PMLZ.ind) = ZDZ(PMLZ.ind) ./ PMLZ.K + PMLZ.Sii; % Application of PML
                
                Vz = Vz + (ZDZ + ZDX) .* Si ./ EM.Rho;   % Update Velocities from derivative of Stresses, Stagger Rho Forward X and Z
                
                %%% Apply Dirichlet boundary conditions, which happens to be easy enough
                [Vx(1,:), Vx(end,:), Vx(:,1), Vx(:,end), Vz(1,:), Vz(end,:), Vz(:,1), Vz(:,end)] = deal(0);
                
                %%% Update the recorded wavefield
                irecs = sub2ind(size(Vz), RCV.IndZ, RCV.IndX);
                Zrec(m,:) = Vz(irecs);
                if nargout >= 2, Xrec(m,:) = Vx(irecs); end
                if nargout >= 3, Exx(m, :) = axial_strain(Sxx(irecs), Szz(irecs), EM.L(irecs), EM.Mu(irecs)); end
                
                %%% Display
                if Si * m < 0.050, continue, end
                % init the display on first iteration
                if m == self.Display
                    xscale = ([0 EM.nx-1]) * EM.dx;
                    zscale = ([0 EM.nz-1]) * EM.dz;
                    h.fig_shot = figure('Name', 'wavmod', 'numbertitle', 'off', 'Position',...
                        [100 100 1400 800]);
                    h.ax_shot = subplot(1, 2, 1, 'Parent', h.fig_shot);
                    h.ax_wave = subplot(1, 2, 2, 'Parent', h.fig_shot);
                    h.im_shot =  imagesc(xscale, [0 self.Rl],  [], 'Parent', h.ax_shot);
                    h.im_wave =  imagesc(xscale, zscale, [], 'Parent', h.ax_wave);
                    colormap(h.fig_shot, 'copper')
                end
                % update the display on subsequent iterations
                if self.Display~=0 && mod(m,self.Display) == 0
                    set(h.im_shot,'CData',Zrec); caxis( h.ax_shot ,[-1 1].*0.05)
                    set(h.im_wave,'CData',Vz);   caxis( h.ax_wave ,[-1 1].*0.05)
                    set(h.ax_shot, 'xlim', xscale, 'ylim', [0 self.Rl])
                    set(h.ax_wave, 'xlim', xscale, 'ylim', zscale)
                    %         FRAME = getframe(h.fig); writeVideo(movieobj,FRAME);
                    drawnow
                end
                if mod(m,self.Verbose)==0, disp(m./Nech*100); end
            end            
        end
    end 
end


function exx = axial_strain(sxx, szz, lambda, mu)
exx = (lambda - szz) ./ (2 .* mu + lambda).^2 - sxx ./ (2 .* mu + lambda);
exx = exx ./ (1 - 1 ./ (2 .* mu + lambda).^2);
end

