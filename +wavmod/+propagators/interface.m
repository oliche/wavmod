classdef interface < handle
    % wavmod.models.interface
    % MIT License
    % Copyright (c) 2016 Olivier Winter
    % https://en.wikipedia.org/wiki/MIT_License
    properties (Abstract= true, Constant=true)
        type  % elastic | acoustic | visco-elastic
        trope % isotropic | anisotropic
        ndims % 2/3
    end
    
    properties (Abstract)
        RCV    % structure of receivers to sample the wavefield
        SRC    % wavmod.source class describing source parameters
        EM     % object of type wavmod.models
        OP     % Derivative operator
        Si     % sample interval secs for simulation
        Si_out % sample interval secs for output
        Rl     % record length
        Nech   % number of samples of the simulation
        Nech_out % number of samples of the output
    end
    
    methods
        function init(self)
            % Grab default Si as a function of the model
            if isnan(self.Si), self.Si= self.EM.Si;  end
            % Compute NEch
            self.Nech = ceil(self.Rl/self.Si);
            self.Nech_out = ceil(self.Rl/self.Si);
            % handle the source/receiver parameters
            if isempty(self.SRC),
                error('no source')
            end
            if isempty(self.RCV)
                error('no receiver')
            end
            % check dx/dz
            Lmin = self.EM.vmin ./ self.SRC.Freq;
            if ~all( [self.EM.dx, self.EM.dz]*10 < Lmin)
                warning(['The grid size ' num2str([self.EM.dx self.EM.dz]) ' may not match the slowest velocity ' ...
                    num2str(self.EM.vmin) ' m/s, would recommend : ' num2str(Lmin/10) 'm spacings'])
            end
            % Compute source and receiver indices relative to model
            self.RCV.ComputeIndices(self.EM);
            self.SRC.ComputeIndices(self.EM);
        end
    end
end