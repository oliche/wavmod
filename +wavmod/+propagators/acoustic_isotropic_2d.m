classdef acoustic_isotropic_2d < wavmod.propagators.interface
    % wavmod.propagator.elastic_isotropic
    % MIT License
    % Copyright (c) 2016 Olivier Winter
    % https://en.wikipedia.org/wiki/MIT_License
    properties (Constant)
        type = 'acoustic'    % elastic | acoustic | visco-elastic
        trope= 'isotropic'  % isotropic | anisotropic
        ndims = 2
    end
    
    properties
        RCV             % structure of receivers to sample the wavefield
        SRC             % structure describing source parameters
        EM              % object of type wavmod.models
        OP              % Derivative operator
        Si = NaN        % sample interval secs for simulation
        Si_out = 0.002  % sample interval secs for output
        Rl     = 4      % record length
        Nech
        Nech_out
        % ----------------------------------------
        orderSpace = 5; % derivative order space
        orderTime = 1; % derivative order time domain
        npml= 15   % size of the perfectly matched layer
        Display = 0 % rate of display
        Verbose = 200 % rate of terminal update
    end
    
    methods
        function self = acoustic_isotropic_2d(varargin)
            % acoustic_isotropic_2d()
            %% input arguments
            p=inputParser;
            p.addParamValue('EM',  [] , @(x) isa(x, 'wavmod.models.acoustic_isotropic') );
            p.addParamValue('RCV', self.RCV);
            p.addParamValue('SRC', self.SRC, @(x) isa(x, 'wavmod.source'));
            p.addParamValue('orderSpace', self.orderSpace, @isnumeric);
            p.addParamValue('orderTime', self.orderTime, @isnumeric);
            p.addParamValue('Si',    self.Si, @isnumeric);
            p.addParamValue('Si_out',    self.Si_out , @isnumeric);
            p.addParamValue('Rl',    self.Rl , @isnumeric);
            p.addParamValue('npml',  self.npml, @isnumeric);
            p.addParamValue('Display',  self.Display , @isnumeric);
            p.addParamValue('Verbose',  self.Verbose, @isnumeric);
            p.parse(varargin{:});
            for ff=fields(p.Results)', eval(['self.' ff{1} '=p.Results.' ff{1} ';' ]); end
             % Grab a default model if necessary
            if isempty(self.EM), EM =  wavmod_tests.models_library('Marmousi'); end
            % Once we have a model, init
            self.init();
            % Derivative Operator
            self.OP = wavmod.pde2.derivativeOperator(self.orderSpace,2);
        end
        
        function init(self)
            % Invoke the superclass init
            init@wavmod.propagators.interface(self);
        end        
        
        function [Zrec] = run(self)
            %% For a lighter code below, short the self
            SRC = self.SRC;
            RCV = self.RCV;
            OP = self.OP;
            EM = self.EM;
            npml = self.npml;
            Si = self.Si;
            Nech = self.Nech;
            Ntr = length(RCV.X);
            C=(EM.Vp.*Si./EM.dz).^2;
            %% Prepare
            [Wa, Wz, Wb] = deal(EM.Vp.*0);
            Zrec = zeros(Nech,Ntr,'single'); 
            %% Loop over time
            for m = 1:(Nech)
                %%% Apply Source Condition
                if m <= length(SRC.WavZ)
                    assert( all( [length(SRC.Z), length(SRC.X)]<=1)) % TODO : non spatially discrete sources
                    Wz(SRC.IndZ,SRC.IndX)= Wa(SRC.IndZ,SRC.IndX) + SRC.WavZ(m) .* SRC.AmpZ;
                end
                %%% Finite difference computation
                Wb = 2.*Wa - Wz + C.* (filter2(OP ,Wa,'same'));
                Wz=Wa;
                Wa=Wb;
                %%% Apply Dirichlet boundary conditions, which happens to be easy enough
                [Wa(1,:), Wa(end,:), Wa(:,1), Wa(:,end), Wa(1,:), Wa(end,:), Wa(:,1), Wa(:,end)] = deal(0);
                %%% Update the recorded wavefield
                Zrec(m,:) = Wa(sub2ind( size(Wa), RCV.IndZ, RCV.IndX ));
                %%% Display
                if Si*m < 0.050, continue, end
                if self.Display~=0 && mod(m,self.Display) ==0
                    ViewData(Zrec,Si,'Simul') % TODO remove links to library
%                     set(h.im_shot,'CData',Zrec); caxis( h.ax_shot ,[-1 1].*0.05)
%                     set(h.im_wave,'CData',Vz);   caxis( h.ax_wave ,[-1 1].*0.05)
                    %         FRAME = getframe(h.fig); writeVideo(movieobj,FRAME);
                    drawnow
                end
                if mod(m,self.Verbose)==0, disp(m./Nech*100); end
            end            
        end
    end
    
    
    
    
    
    
end