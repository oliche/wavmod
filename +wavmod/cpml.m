classdef cpml
    % MIT License
    % Copyright (c) 2016 Olivier Winter
    % https://en.wikipedia.org/wiki/MIT_License
    % cpml(EM,npml,Si,Fdom,dim);
    % cpml initialization ; based from this very useful paper :
    % ! @ARTICLE{KoMa07,
    % ! author = {Dimitri Komatitsch and Roland Martin},
    % ! title = {An unsplit convolutional {P}erfectly {M}atched {L}ayer improved
    % !          at grazing incidence for the seismic wave equation},
    % ! journal = {Geophysics},
    % ! year = {2007},
    % ! volume = {72},
    % ! number = {5},
    % ! pages = {SM155-SM167},
    % ! doi = {10.1190/1.2757586}}
    
    properties
        ind
        K
        a
        b
        Vx
        Vz
        Sii
        Sij
        iab
    end
    methods
        %% Constructor
        function self = cpml(EM,PROP,dim);
            % unpack arguments for lisibility
            nz = EM.nz;
            nx = EM.nx;
            ny = EM.ny;
            dx = EM.dx;
            Fdom = PROP.SRC.Freq;
            npml = PROP.npml;
            EM.ndim;
            %% default parameters (can be made optional later ?)
            damping = @(N,vp,Rc,L) -(N-1).*vp.*log(Rc)./(2.*L); %inital damping formula as a function of P velocity, reflection coeff, PML size
            dmp0 = damping(2,3300*3,0.001,dx.*npml); % compute inital damping
            K  = 1 ;
            alpha_max = Fdom.*pi; % alpha max = pi.*dominant_frequency
            taper = [sin(linspace(0,pi/2,npml))]; taper = [taper, fliplr(taper)]'; % use a sine taper for alpha
            dmp = dmp0.* ([npml:-1:1 1:npml ]'./npml).^2; % hyperbolic decrease of attenuation in PML
            %% Compute PML indices
            switch dim
                case 1
                    ind = [1:npml , nz-npml+1:nz]' ; % 1D index over z
                    [i2,i1,i3] = meshgrid(1:nx,ind,1:ny);    % Propagate in 3D
                    iab = i1(:); iab(iab > nz-npml) = -iab(iab > nz-npml)+nz+1; % indab maps the PML memory terms to a and b
                case 2
                    ind = [1:npml , nx-npml+1:nx]  ; %1D index over x
                    [i2,i1,i3] = meshgrid(ind,1:nz,1:ny);    % Propagate in 3D
                    iab = i2(:); iab(iab > nx-npml) = -iab(iab > nx-npml)+nx+1; % indab maps the PML memory terms to a and b
                case 3
                    ind = [1:npml , nz-npml+1:ny]' ; % 1D index over y
                    [i2,i1,i3] = meshgrid(1:nx,1:nz,ind);    % Propagate in 3D
                    iab = i1(:); iab(iab > ny-npml) = -iab(iab > ny-npml)+ny+1; % indab maps the PML memory terms to a and b
            end
            self.ind = sort(sub2ind([nz,nx,ny], i1(:),i2(:),i3(:))); % ind maps the full volume to the PML memory terms
            self.b = exp(-(alpha_max.*taper + dmp/K).*PROP.Si);
            self.a = dmp./(K.*(dmp + K.*alpha_max.*taper)).*(self.b-1);
            self.K = K;
            self.iab = iab;
            % Create the pml structure 
            switch EM.ndim
                case 2,
                    [self.Vx, self.Vz , self.Sii , self.Sij] = deal( self.ind.*0 );
                case 3, cpml3d;
                    error('TODO 3D memory terms')
                    [self.Vx, self.Vz , self.Sii , self.Sij] = deal( self.ind.*0 );
            end
        end
        %% Getter for a
        function a = A(self,value)
            a = self.a(self.iab);
        end
        %% Getter for b
        function b = B(self,value)
            b = self.b(self.iab);
        end
    end
end




