classdef source < wavmod.transducer
% MIT License
% Copyright (c) 2016 Olivier Winter
% https://en.wikipedia.org/wiki/MIT_License
% SRC = wavmod.source('Freq', 15, 'WavZ', ricker(15,MOD.Si), 'X', 2000, 'Y', 0, 'Z', (1+npml)*MOD.dz, 'AmpX', 0, 'AmpY',0,'AmpZ',1)
   properties
        Freq = 15;
        WavX = [];
        WavY = [];
        WavZ = [];
        X   = 0;
        Z   = 0;
        Y   = 0;
        AmpX = 0; 
        AmpY = 0;
        AmpZ = 1; 
        Si   = NaN;
        Nech = NaN;
        IndX = []; % to be filled by a propagator run with a model
        IndZ = []; % to be filled by a propagator run with a model
        IndY = []; % to be filled by a propagator run with a model
   end
    
   methods
       %% Constructor
       function self = source(varargin)
           % Handle Options input
           p=inputParser;
           p.addParamValue('Freq', self.Freq, @isnumeric);
           p.addParamValue('WavX', self.WavX, @isnumeric);
           p.addParamValue('WavY', self.WavY, @isnumeric);
           p.addParamValue('WavZ', self.WavZ, @isnumeric);
           p.addParamValue('X',    self.X , @isnumeric);
           p.addParamValue('Y',    self.Y , @isnumeric);
           p.addParamValue('Z',    self.Z , @isnumeric);
           p.addParamValue('AmpX', self.AmpX , @isnumeric);
           p.addParamValue('AmpY', self.AmpY , @isnumeric);
           p.addParamValue('AmpZ', self.AmpZ , @isnumeric);
           p.addParamValue('Si',   self.Si , @isnumeric);
           p.parse(varargin{:});
           for ff=fields(p.Results)', eval(['self.' ff{1} '=p.Results.' ff{1} ';' ]); end
           % default wavelet is a 3D Ricker
           if isempty(self.WavZ),
               [self.WavZ,self.WavY,self.WavX] = deal(ricker(self.Freq,self.Si));
           end
           self.Nech = length(self.WavZ);
       end
       %% SuperClass Methods
       function ComputeIndices(self,EM)
            ComputeIndices@wavmod.transducer(self,EM);
       end
   end
end