function M = models_library(tag)
% M =  wavmod_tests.models_library(tag)
% M = wavmod_tests.models_library('CREWES_layers')
% M = wavmod_tests.models_library('Marmousi2'); % elastic Marmousi
% M = wavmod_tests.models_library('Marmousi'); % Acoustic Marmousi

chem = fileparts(which('wavmod_tests.models_library'));
chem = [chem, filesep, 'data_models', filesep ];

typ = 'LambdaMu';
switch tag
    case 'homogeneous' %% Model Homogene CPML
        dx = 10;
        nz = 101; nx = 641;  vp = 3300; vs = 3300/sqrt(3);
        [L, Mu, Rho] = deal(zeros(nz,nx));
        Rho = Rho +2.800;
        Mu = Mu + (vs).^2.*Rho;
        L = L + (vp.^2 - 2.*vs.^2 ).*Rho;
    case 'CREWES_layers'
        load([chem, filesep, 'CREWES_Layers.mat']);
        Rho = Rho';
        Mu = Mu';
        L  = (Lp2m'-2.*Mu);
        dx=2; 
    case 'Marmousi2'
        % elastic isotropic upgrade to the original Marmousi
        % http://www.agl.uh.edu/downloads/downloads.htm
        typ = 'VpVs';
        load([chem, filesep, 'Marmousi2.mat']);
        dx = 1.25;
        Vp = Vp.*1000;
        Vs = Vs.*1000;
    case 'Marmousi'
        typ = 'Vp';
        load([chem, filesep, 'Marmousi2.mat']);
        dx = 1.25;
        Vp = Vp.*1000;
end

switch typ
    case 'LambdaMu'
        M = wavmod.models.elastic_isotropic(dx, Rho, 'L',L,'Mu',Mu);
    case 'VpVs'
        M = wavmod.models.elastic_isotropic(dx, Rho, 'Vp',Vp,'Vs',Vs);
    case 'Vp'
        M = wavmod.models.acoustic_isotropic(dx, Rho, Vp);
end

