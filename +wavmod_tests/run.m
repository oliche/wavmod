function res = run

suite = matlab.unittest.TestSuite.fromPackage('wavmod_tests');
res   = run(suite);
