classdef models__elastic_isotropic_test < matlab.unittest.TestCase
    
    properties
        AM
    end
    
    methods(TestMethodSetup)
        function createModel(testCase)
            %% Constructor
            Vp  = kron([1800 1900; 2000 2100], ones(4));
            Rho = kron([1.8 1.9; 2.0 2.1], ones(4));
            Vs  = kron([1800 1900; 2000 2100]*0.6, ones(4));
            dx = 5;
            testCase.AM = wavmod.models.elastic_isotropic(dx, Rho, 'Vp',Vp,'Vs',Vs);
        end
    end
    
    methods(TestMethodTeardown)
        function closeFigure(testCase)
            close('all')
        end
    end
    
    methods(Test)
        
        function testSi(self)
            %% Sampling interval
            self.AM.Si;
            assert(true)
        end
        
        function testResample(self)
            %% Resampling
            self.AM.resample_interp([2.5]);
            assert(all(size(self.AM.Rho)==[self.AM.nz self.AM.nx]));
            assert(all(size(self.AM.Rho)==[15 8]));
            self.AM.resample_interp([2.5 2.5]);
            assert(all(size(self.AM.Rho)==[self.AM.nz self.AM.nx]));
            assert(all(size(self.AM.Rho)==[15 15]));
        end
        
        function testAddWhiteNoise(self)
            %% Add white noise
            self.AM.addWhiteNoiseShallow(10, 20);
            % try using a maximum depth that exceeds the actual depth of the model
            self.AM.addWhiteNoiseShallow(200, 20);
            assert(true);
        end
        
        function testShowModel(self)
            %% Show Model
            self. AM.ShowModel;
        end
        
        function testModFcn(self)
            nzin = self.AM.nz;
            self.AM.modfun(@(x) x(1:end-1,:))
            assert(self.AM.nz == nzin-1)
            assert(size(self.AM.Rho,1) ==nzin-1)
        end
    end
    
end





