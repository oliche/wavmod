%% Mod�le Br�sil : mod�le 1D tir� de la premi�re trace de Marmousi 2 
MOD = wavmod_tests.models_library('Marmousi2')
din = MOD.dz;

Xmax = 10000; Zmax = 4250; d= 3.75; d = 10;
nx_out = round(Xmax/d);
nz_out = round(Zmax/d);

MOD.modfun(@(x) x(400:end,1)) % this is a 1D model, pick only the first trace, remove the water layer
MOD.resample_interp([d d])  % resample to desired spatial interval in depth
% pad the depth to desired length, repmat on the x direction
modelfcn = @(x) repmat( [x(1:min(size(x,1),nz_out)) ; ones( nz_out - (size(x,1)) ,1) .* x(end,1)] , 1, nx_out)
MOD.modfun(modelfcn)
% MOD.addWhiteNoiseShallow(100,20)
MOD.ShowModel({'Vp','Vs'})



%% Maintenant lancer une simulation
npml = 30;
Si = MOD.Si; % sampling interval for the simulation
RCV = wavmod.receiver('Z', zeros(MOD.nx,1)+((npml+1)*MOD.dz) ,'X', [1:MOD.nx]'.*MOD.dx );
SRC = wavmod.source('Freq', 15, 'Si',Si,'X', 2000, 'Y', 0, 'Z', (1+npml)*MOD.dz, 'AmpX', 0, 'AmpY', 0, 'AmpZ',1);
Propagator = wavmod.propagators.elastic_isotropic_2d('EM',MOD,'SRC',SRC,'RCV',RCV,'Display',200);
%%
tic, W = Propagator.run(); toc


% SIMUL01 : fac 2, 350 secs
% SIMUL02 : fac10, 36494 secs
% SIMUL03 : fac 4, 2896 secs
% SIMUL04 : fac 4, 3384 secs, backscatter
