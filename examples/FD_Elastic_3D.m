%% Mod�le Br�sil : mod�le 1D tir� de la premi�re trace de Marmousi 2 
MOD = wavmod_tests.models_library('Marmousi2')
d = 10;
MOD.resample_interp([d d]);  % resample to desired spatial interval in depth
modelfcn  = @(x) repmat(x(:,1:500),[1 1 500]);
MOD.modfun(modelfcn);
MOD.ndim = 3; MOD.dy = d;
% MOD.addWhiteNoiseShallow(100,20)
MOD.ShowModel({'Vp','Vs'})



%% Maintenant lancer une simulation
npml = 30;
Si = MOD.Si; % sampling interval for the simulation
[A,R]= meshgrid( linspace(0,2.*pi,100) , linspace(0,2500,10) );
rxy = flatten(R.*exp(i.*A)) + (MOD.nx.*MOD.dx)/2 + i.*(MOD.ny.*MOD.dy)/2;

RCV = wavmod.receiver('Z', rxy.*0 +((npml+1)*MOD.dz) ,'X', real(rxy), 'Y',imag(rxy) ); % dans ce cas on enregistre toute la surface
SRC = wavmod.source('Freq', 15, 'Si',Si,'X', 2000, 'Y', 2000, 'Z', (1+npml)*MOD.dz, 'AmpX', 0, 'AmpY', 0, 'AmpZ',1);

Propagator = wavmod.propagators.elastic_isotropic_3d('EM',MOD,'SRC',SRC,'RCV',RCV,'Display',200);

figure,plot(RCV.X,RCV.Y,'.'), axis equal, hold on
plot(SRC.X,SRC.Y,'r*')
plot( [0 0 MOD.nx  MOD.nx 0].*MOD.dx,  [0 MOD.ny  MOD.ny 0 0].*MOD.dy, 'g', 'linewidth',2)

%%
tic, W = Propagator.run(); toc
