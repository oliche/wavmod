%% Model Virieux
RL=3; % record length
[dx, dz] = deal(20) ;
x = 0:dx:10000; x = x-mean(x);
z = 0:dz:8000 ;
[X,Z] = meshgrid(x,z);

% Weathered layer model
[Vp Vs Rho] = deal(X.*0 );
Vp = Vp + 6000;
Vp(Z<=2250) = 2000;
Vs = Vs + 4200;
Vs(Z<=2250) = 1600;
Rho = Rho + 2.5;

% choose a proper time step
ndims = 2; fmax = 40;
% Virieux 1986
Si = min(flatten(dx./(Vp.*sqrt(ndims)))).*0.8;
Si = min(Si , min(flatten(dx./abs(Vp+i.*Vs))).*0.8);
% Xu 2005
Si = 0.6*min(dx,dz)./max(Vp(:))
% assert(max(dx,dz) <= 0.125.* min(Vp(:))./fmax )

% change of variables on staggered grid : from Vp,Vs,RHo, to Buyancy, Lambda, Mu
Mu = Vs.^2.*Rho;
L  = Vp.^2.*Rho - 2.*Mu;
SRC = struct('Wav',ricker(40,Si),'IndX',250,'IndZ',6,'AmpX',0,'AmpZ',1);
% Model CREWES
% load('C:\Users\owinter\Documents\MATLAB\CREWES_Layers.mat');
% dx = 2;
% EM = wavmod.earthModel_Elastic_Iso(dx, Rho', 'L', (Lp2m'-2.* Mu') ,'Mu',Mu'); % Lam� parameters
% SRC = struct('Wav',force,'IndX',[500,501],'IndZ',6,'AmpX',0,'AmpZ',1);

%%
% Compute the convolution operator for derivatives
order = 1;
OP.dxdz = convn([-1,1],[-1;1]);  %dxdz : order 1
OP.dxdx= wavmod.pde2.derivativeOperator(order)'; % second derivative along x
OP.dzdz= wavmod.pde2.derivativeOperator(order) ; % second derivative along z
% Init arrays
Nech = ceil(RL/Si);
[UXZ, UXZ_ , UXZnew ]  = deal(repmat(Mu.*0, [1 , 1, 2]));

for m = 1:(Nech)
    % Application of the source function
    if m <= length(SRC.Wav)
        UXZ(SRC.IndZ,SRC.IndX,1)= UXZ(SRC.IndZ,SRC.IndX,1) + SRC.Wav(m) .* SRC.AmpX;
        UXZ(SRC.IndZ,SRC.IndX,2)= UXZ(SRC.IndZ,SRC.IndX,2) + SRC.Wav(m) .* SRC.AmpZ;
    end
    % Compute the derivatives
    UXZdxdx = convn(UXZ,OP.dxdx,'same');
    UXZdzdz = convn(UXZ,OP.dzdz,'same');
    UXZdxdz = convn(UXZ,OP.dxdz,'same');
%     if m<=3,
%         aaa = 0;
% %         UXZ(3:8,497:504,2)'*1e3
% %         UXZ(3:8,497:504,1)'*1e3
%         keyboard;
%         if aaa ==1, break, end
%     end
    % Compute the new displacements
    UXZnew(:,:,1) = 2.*UXZ(:,:,1) - UXZ_(:,:,1) + (Si.*Si/dx/dz./Rho) .* ...
        ( (L+2.*Mu).* UXZdxdx(:,:,1) + (L+Mu).*UXZdxdz(:,:,2) + Mu.*UXZdzdz(:,:,1) );
    UXZnew(:,:,2) = 2.*UXZ(:,:,2) - UXZ_(:,:,2) + (Si.*Si/dx/dz./Rho) .* ...
        ( (L+2.*Mu).* UXZdzdz(:,:,2) + (L+Mu).*circshift(circshift(UXZdxdz(:,:,1),1,1),1,2) + Mu.*UXZdxdx(:,:,2));
    % Update variables
    UXZ_ = UXZ;
    UXZ = UXZnew;
    % Display wavefield if necessary
    if mod(m,50)==0
        disp(m./Nech)
        ViewData(UXZ(:,:,2),dz,'Uz');
        drawnow
    end
end

%% Gaussian pulse
t = [0:500].'*Si;
src = @(t,alpha) exp(-alpha.*(t).^2)
dsrc = @(t,alpha) -2.*alpha.*(t).*exp(-alpha.*(t).^2)
% derivative of a Gaussian pulse

figure,plot( t, [src(t,200) dsrc(t,200)])

Spectre([src(t,200) dsrc(t,200)], Si)


%%
A = magic(5)
circshift(convn(A,[-1 1],'same'),1,2)

convn(A,[0 -1 1],'same')
