%% Essai des diff�rences finies
addpath('E:\owinter\Dropbox\2015-06-08_FDMToolbox\Util\')
% Init
Si=0.001; dX=3; dZ=3; L=1;
% cr�ation de la grille
[Z,X]=ndgrid([0:dZ:5000],[0:dX:10000]-mean([0:dX:10000]));
[nz nx]=size(Z);
V=Z.*0;
% cr�ation d'un mod�le de vitesses

V( entre(Z, [ 0   10])  ) =  800*3;
V( entre(Z, [ 10 200])  ) = 1200*3;
V( entre(Z, [200 233])  ) = 1100*3;
V( entre(Z, [233 358])  ) = 1800*3;
V( entre(Z, [358 430])  ) = 2200*3;
V( entre(Z, [430 442])  ) = 2300*3;
V( entre(Z, [442 Inf])  ) = 2900*3;

xcav = [-64.5351361918289;-66.4442678278872;-54.3531007995176;-42.2619337711479;-35.8981616509534;-21.8978629865253;-5.988432686039;-4.07930104998069;-9.80669595815572;-25.716126258642;-60.0804957076927;-25.716126258642;-29.5343895307588;-28.2616351067199;-25.716126258642;-16.1704680783503;-1.53379220190273;13.7392608865642;36.0124633072451;51.285516395712;57.6492885159067;55.1037796678288;46.8308759115759;41.1034810034008;40.4671037913814;53.1946480317704;83.7407542087043;71.6495871803346;50.6491391836926;46.8308759115759;19.4666557947392;18.1939013707003;24.557673490895;46.1944986995565;48.1036303356149;62.7403062120623;60.831174576004;64.6494378481207];
ycav = [1.28617300801831;15.4340760962194;18.6495086162651;27.652719672393;32.7974117044661;27.0096331683839;69.4533424329871;99.6784081214166;131.189646817865;151.125328442148;208.360027298961;231.51114144329;257.877688107665;286.816580788076;329.26029005268;349.839058180972;355.626836717054;356.913009725073;351.12523118899;341.478933628853;318.970905988533;300.321397372268;288.745840300104;279.099542739967;249.517563555546;230.224968435272;205.144594778916;174.919529090486;140.835944378002;129.903473809846;86.1735915372248;66.8809964169505;33.4404982084753;36.655930728521;29.5819791844204;27.652719672393;14.147903088201;0];

iz = inpolygon(X,Z,xcav,ycav);
V(iz) = 300*3;

V((iz & (Z > 227))) = 500*3; % todo granularity

figure,imagesc(X(1,:),Z(:,1),V), colormap('copper')
%%
order = 5;
Si=0.9*dX*EFDMstability(order)/(max(V(:))*sqrt(3));
Nech=ceil(L/Si);
[c0 , c ] =HighOrderCoefs( order);
kernel = [flipud(c(:)) ; c0(:) ;  c(:)];
kernel = cat(2,repmat(kernel.*0,1,order) ,kernel,repmat(kernel.*0,1,order));
OP = kernel + kernel';


%% It�ration
% Cr�ation des matrices champ d'onde
Seis=zeros(Nech,nx);
% creation de la function source: un ricker multidimensionel
sx = -200;
[~,isx] = min(abs(X(1,:) - sx));

Wa = ricker2d(0.006,nx,dX,nz,dZ,isx*dX,6*dZ);
Wz=V.*0; Wb=V*0; 


C=(V.*Si./dX).^2;
for m=1:Nech
    Wb = 2.*Wa - Wz + C.* (filter2(OP ,Wa,'same'));
    Wz=Wa;
    Wa=Wb;
    % %    ici j'avais mis
    %     Wa=Wb;
    %     Wz=Wa;
    % champion du monde...
    Seis(m,:)=Wa(1,:);
    if mod(m,100)==0,
        disp(num2str(m/Nech*100));
        ViewData(Wa,1,'Wa');
        %         ViewData(Z,Si)
    end
end


%%
H = fct_create_H2(Nech,nx,0.000137154332557736);
H(60,:)=X(1,:);
H(62,:)=sx;
H(20,:) = abs(H(60,:)-H(62,:));
h = guidata(ViewData(Seis,H,'order2'));
%
delim = round(interp1(X(1,:),1:nx,[X(1) -325 ;  -73.5 73.5 ; 325 X(end)]));
delim = round(interp1(X(1,:),1:nx,[X(1) -1000 ;  -73.5 73.5 ; 1000 X(end)]));


set(h.figure1,'Renderer','opengl')
for m=1:3;
p(m) = patch( delim(m,[1 2 2 1 1]) , [0 0 L L 0], ones(1,5)/2 ,'Parent',h.axes1, 'FaceColor','r', 'FaceAlpha',0.4)
end
% delete(p)

%%



figure,imagesc(X(1,:),Z(:,1),V), colormap('copper'), colorbar, title('P-wave velocity(ft.s^{-1})')
hold on
rloc = X(1,  entre(abs(X(1,:)),[73.5 325])) ;
plot(rloc,rloc*0  ,'bx' ), axis equal
plot(sx,0,'r*')


%%
save('Florida.mat')


