%% Essai des diff�rences finies
addpath('E:\owinter\Dropbox\2015-06-08_FDMToolbox\Util\')
% Init
Si=0.001; dX=30; dZ=30; L=2.2;
% cr�ation de la grille
[Z,X]=ndgrid([0:dZ:15000],[0:dX:60000]-mean([0:dX:30000]));
[nz nx]=size(Z);

% cr�ation d'un mod�le de vitesses
% V=zeros(nz,nx)+2500*3;
% V(Z<100)=1500*3;
% V(Z>=1000 & X>0)=3000*3;
% V(Z>=1500 & X<=0)=3000*3;
% oupas
V = ss_ech(Vlaw.Vint, dZ./unique(diff(Vlaw.Depth)));
V = [V; zeros(nz - length(V),1) + V(end)];
V = repmat(V,1,nx); 

order = 5;
Si=0.9*dX*EFDMstability(order)/(max(V(:))*sqrt(3));
Nech=ceil(L/Si);
% op�rateur des indices � convoluer
% OP=[0 1 0; 1 -4 1; 0 1 0] ;
[c0 , c ] =HighOrderCoefs( order);
kernel = [flipud(c(:)) ; c0(:) ;  c(:)];
kernel = cat(2,repmat(kernel.*0,1,order) ,kernel,repmat(kernel.*0,1,order));
OP = kernel + kernel';
% randomization of the velocity model for the heck of it
%  V=V+ V.*0.05.*randn(size(V)) ;

%% It�ration
% Cr�ation des matrices champ d'onde
Z=zeros(Nech,nx);
% creation de la function source: un ricker multidimensionel
Wa = ricker2d(0.006,nx,dX,nz,dZ,nx/2*dX,6*dZ);
Wz=V.*0; Wb=V*0; 


C=(V.*Si./dX).^2; 
for m=1:Nech
    Wb = 2.*Wa - Wz + C.* (filter2(OP ,Wa,'same'));
    Wz=Wa;
    Wa=Wb;
% %    ici j'avais mis
%     Wa=Wb;
%     Wz=Wa;
% champion du monde...
    Z(m,:)=Wa(1,:);
    if mod(m,100)==0, disp(num2str(m/Nech*100)); end
end


%%
H = fct_create_H2(Nech,nx,Si);
H(60,:)=[0:dX:60000];
H(62,:)=nx/2*dX;
H(20,:) = abs(H(60,:)-H(62,:));
ViewData(Z,H,'order2')

Si

%%
% 2900 FT ASL R.V: 13600 FT/S  
t0 = 2900/13600;

[ZZ] = Nmo(Z,H(20,:)./3,Si,Vlaw.Tw,Vlaw.Vrms./3,1,30);
ViewData(ZZ,H,'1')
[ZZ] = Nmo(Z,H(20,:)./3,Si,tscale-t0,mean(W,2)/3,1,30);
ViewData(ZZ,H,'wv')





