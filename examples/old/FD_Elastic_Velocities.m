%% Model choice
EM = wavmod_tests.models_library('CREWES_layers'); RL = 1; Fdom = 25;
EM = wavmod_tests.models_library('Marmousi2'); RL =4; Fdom = 15;


wavmod.propagators.elastic_isotropic_2d

%% Init PML
% One example to constrain Si for stability (Courant-Friedrichs-Lewy, 1928)
Si = EM.dx./sqrt(2)/ max(flatten(EM.Vp)) *0.8;
% Another example to constrain dx (CREWES):
Lmin = min(flatten(nonzeros(EM.Vs))) ./ Fdom;
% assert( all( [EM.dx, EM.dy , EM.dz] < Lmin/10))
npml = 15;
% SRC = struct('Wav',force,'IndX',[500,501],'IndZ',npml +1,'AmpX',0,'AmpZ',1);
PMLX = wavmod.cpml(EM, npml, Si , Fdom, 1);
PMLZ = wavmod.cpml(EM, npml, Si , Fdom, 2);
force = ricker(Fdom,Si);
SRC = struct('Freq',Fdom,'Wav',force,'IndX',round(EM.nx/2) + [0 1],'IndZ',npml +1,'AmpX',0,'AmpZ',1);
GEOS = cat(2, [1:EM.nx]'.*0+ npml +1 ,  [1:EM.nx]'); % todo refactor this properly with coordinates
%%
OP = struct('dxf',[-1 1], 'dzf', [ -1 ;1], 'dxb',[0 -1 1], 'dzb', [0; -1 ; 1]);
[Sxx, Szz, Sxz, Vx, Vz, XDX, ZDZ, XDZ, ZDX] = deal(EM.Mu.*0);
Nech = ceil(RL/Si);
% create the recorded data array
Zrec = zeros(Nech, size(GEOS,1));
H = fct_create_H2(Nech,size(GEOS,1),Si);
H(60,:) = GEOS(:,2).*EM.dx;
H(62,:) = mean(SRC.IndX).*EM.dx;
H(20,:) = abs(H(60,:)-H(62,:));
% Display
h.fig = figure('Position',[200,200, 1600, 600]); colormap('pink');
h.ax_wave = subplot(1,5,[1:3],'parent',h.fig); h.im_wave = imagesc(EM.xscale, EM.zscale, Vz);
set(h.ax_wave,'nextplot','add') , h.pl_recs = plot(h.ax_wave, GEOS(:,2).*EM.dx, GEOS(:,1).*EM.dz, 'k.')
h.ax_shot = subplot(1,5,[4 :5],'parent',h.fig); h.im_shot = imagesc(GEOS(:,2).*EM.dx, [1:Nech].*Si, Zrec)
% movieobj = VideoWriter('E:\owinter\Presentations\2016\Marmousi.avi'); 
% set(movieobj, 'FrameRate',80); open(movieobj)
% Loop over time
for m = 1:(Nech)
    %%% Apply Source term
    if m <= length(SRC.Wav)
        Vx(SRC.IndZ,SRC.IndX)= Vx(SRC.IndZ,SRC.IndX) + SRC.Wav(m) .* SRC.AmpX;
        Vz(SRC.IndZ,SRC.IndX)= Vz(SRC.IndZ,SRC.IndX) + SRC.Wav(m) .* SRC.AmpZ;
    end
    
    %%% Compute stresses
    % Compute spatial derivatives of velocities
    XDX = convn(Vx,OP.dxf,'same') ./ EM.dx;  % d�riv�e spatiale de la vitesse x selon x NO SHIFT (forward)
    ZDZ = convn(Vz,OP.dzb,'same') ./ EM.dz;  % d�riv�e spatiale de la vitesse z selon z SHIFT    (backward)
    XDZ = convn(Vx,OP.dzf,'same') ./ EM.dz;  % d�riv�e spatiale de la vitesse x selon z NO SHIFT (forward)
    ZDX = convn(Vz,OP.dxb,'same') ./ EM.dx;  % d�riv�e spatiale de la vitesse z selon x SHIFT    (backward)
    
    PMLX.Vx =  PMLX.Vx .* PMLX.B + PMLX.A.*XDX(PMLX.ind) ; % PML memory term % Stagger ax,bx X Forward
    PMLZ.Vz =  PMLZ.Vz .* PMLZ.B + PMLZ.A.*ZDZ(PMLZ.ind) ; % PML memory term % No Stagger
    PMLX.Vz =  PMLX.Vz .* PMLX.B + PMLX.A.*ZDX(PMLX.ind) ; % PML memory term % No Stagger
    PMLZ.Vx =  PMLZ.Vx .* PMLZ.B + PMLZ.A.*XDZ(PMLZ.ind) ; % PML memory term % Stagger az, bz Z Forward
    
    XDX(PMLX.ind) = XDX(PMLX.ind) ./ PMLX.K + PMLX.Vx ;  % Application of PML % Stagger Kx, X Forward
    ZDZ(PMLZ.ind) = ZDZ(PMLZ.ind) ./ PMLZ.K + PMLZ.Vz ;  % Application of PML % No Stagger
    ZDX(PMLX.ind) = ZDX(PMLX.ind) ./ PMLX.K + PMLX.Vz ;  % Application of PML % No Stagger
    XDZ(PMLZ.ind) = XDZ(PMLZ.ind) ./ PMLZ.K + PMLZ.Vx ;  % Application of PML % Stagger Kz, Z forward
    
    Sxx = Sxx + ((EM.L + 2.*EM.Mu) .* XDX + EM.L.*ZDZ ) .* Si ;  % Update stresses from derivative of velocities % Stagger L,Mu Forward X
    Szz = Szz + ((EM.L + 2.*EM.Mu) .* ZDZ + EM.L.*XDX ) .* Si ;  % Update stresses from derivative of velocities % Stagger L,Mu Forward X
    Sxz = Sxz +  (EM.Mu       ) .*(XDZ + ZDX)     .* Si;   % Update stresses from derivative of velocities % Stagger Mu   Forward Y
    
    %%% COmpute velocities : Vx
    XDX = convn(Sxx,OP.dxb,'same') ./ EM.dx; % Compute spatial derivatives of stresses SHIFT (backward)
    XDZ = convn(Sxz,OP.dzb,'same') ./ EM.dz; % Compute spatial derivatives of stresses SHIFT (backward)
    
    PMLX.Sii =  PMLX.Sii .* PMLX.B + PMLX.A.*XDX(PMLX.ind) ; % PML memory term
    PMLZ.Sij =  PMLZ.Sij .* PMLZ.B + PMLZ.A.*XDZ(PMLZ.ind) ; % PML memory term
    XDX(PMLX.ind) = XDX(PMLX.ind) ./ PMLX.K + PMLX.Sii; % Application of PML
    XDZ(PMLZ.ind) = XDZ(PMLZ.ind) ./ PMLZ.K + PMLZ.Sij; % Application of PML

    Vx = Vx + (XDX + XDZ) .* Si ./ EM.Rho;   % Update Velocities from derivative of Stresses, No Stagger Rho
    
    %%% COmpute velocities : Vz
    ZDZ = convn(Szz,OP.dzf,'same') ./ EM.dz; % Compute spatial derivatives of stresses NO SHIFT (forward)
    ZDX = convn(Sxz,OP.dxf,'same') ./ EM.dx; % Compute spatial derivatives of stresses NO SHIFT (forward)
    
    PMLX.Sij =  PMLX.Sij .* PMLX.B + PMLX.A.*ZDX(PMLX.ind) ; % PML memory term
    PMLZ.Sii =  PMLZ.Sii .* PMLZ.B + PMLZ.A.*ZDZ(PMLZ.ind) ; % PML memory term
    ZDX(PMLX.ind) = ZDX(PMLX.ind) ./ PMLX.K + PMLX.Sij; % Application of PML
    ZDZ(PMLZ.ind) = ZDZ(PMLZ.ind) ./ PMLZ.K + PMLZ.Sii; % Application of PML
    
    Vz = Vz + (ZDZ + ZDX) .* Si ./ EM.Rho;   % Update Velocities from derivative of Stresses, Stagger Rho Forward X and Z
    
    %%% Apply Dirichlet boundary conditions, which happens to be easy enough
    [Vx(1,:), Vx(end,:), Vx(:,1), Vx(:,end), Vz(1,:), Vz(end,:), Vz(:,1), Vz(:,end)] = deal(0);
    
    %%% Update the recorded wavefield
    Zrec(m,:) = Vz(sub2ind( size(Vz), GEOS(:,1), GEOS(:,2)) );
    %%% Display
    if Si*m < 0.050, continue, end
    if mod(m,20)==0
        disp(m./Nech)
        set(h.im_shot,'CData',Zrec); caxis( h.ax_shot ,[-1 1].*0.05)
        set(h.im_wave,'CData',Vz);   caxis( h.ax_wave ,[-1 1].*0.05)
%         FRAME = getframe(h.fig); writeVideo(movieobj,FRAME);
        drawnow
    end
end
% movieobj.close;


%%



    